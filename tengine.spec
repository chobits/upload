%define __arch_install_post %{nil}

%define nginx_name      tengine
%define nginx_user      admin
%define nginx_group     %{nginx_user}
%define nginx_home      /home/%{nginx_user}
%define nginx_datadir   %{nginx_home}/cai
%define nginx_sbindir   %{nginx_datadir}/bin
%define nginx_logdir    %{nginx_datadir}/logs
%define nginx_confdir   %{nginx_datadir}/conf
%define nginx_home_tmp  %{nginx_datadir}/data
%define nginx_libdir    %{nginx_datadir}/lib
%define nginx_modules   %{nginx_home_tmp}/modules
%define nginx_include   %{nginx_home_tmp}/include

Name:           %{nginx_name}
Version:1.4.9
Release:        ?
Packager:       weiyue <weiyue@taobao.com>
Summary:        Robust, small and high performance http and reverse proxy server
Group:          System Environment/Daemons
Source:         %{name}-%{version}.tar.gz

# BSD License (two clause)
License:        BSD
URL:            http://svn.simba.taobao.com/svn/nginx/trunk/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:      pcre-devel,zlib-devel,openssl-devel
Requires:           pcre,zlib,openssl
# for /usr/sbin/useradd
Requires(pre):      shadow-utils
Requires(post):     chkconfig
# for /sbin/service
Requires(preun):    chkconfig, initscripts
Requires(postun):   initscripts

# Source0:    http://sysoev.ru/nginx/nginx-%{version}.tar.gz
Source1:    %{name}.conf
Source2:    nginxctl
#this is mod_trans data config
Source3:    ip.dat
Source4:    sm2tr.txt
#this is mod_beacon data config
Source5:    taobao-beacon.cfg
Source6:    taobao-channel.cfg
#these are configure for gray module
Source7:    tengine-gray.conf
Source8:    nginx-gray.lua
#lua json lib
Source9:    json.lua
#resource files for gray module
Source12:   geo_taobao.conf
Source13:   geo_beijing.conf
Source14:   geo_hangzhou.conf
Source15:   geo_guangzhou.conf
Source16:   geo_shanghai.conf
Source17:   geo_shenzhen.conf
Source19:   %{name}.init
Source20:   %{name}.sysconfig
#tair restful
Source21:   tair.lua
#tmd
Source22:   tmd3_main.conf
Source23:   tmd3_http.conf
Source24:   tmd3_ip.conf
Source25:   tmd3_loc.conf
Source26:   tmd3_loc_wireless.conf
#nginx admin
Source27:   nginx-admin.conf
Source28:   nginx-admin-action.lua
Source29:   nginx-admin-utils.lua
Source30:   nginx-admin-ctl
#new gray
Source31:   gray_admin.lua
Source32:   gray_conf.lua
Source33:   gray_utils.lua
Source34:   gray_engine.lua
Source35:   engine_init.lua
Source36:   engine_conf_creator.lua
Source37:   nginx_conf_parser.lua
Source38:   nginx_operator.lua
Source39:   gray-log-format.conf
Source40:   geo_guizhou.conf
Source41:   geo_hubei.conf
Source42:   geo_jiangsu.conf
Source43:   geo_tianjin.conf
Source44:   geo_yunnan.conf
Source45:   geo_henan.conf
Source46:   geo_hunan.conf
Source47:   geo_jilin.conf
Source48:   geo_zhejiang.conf

Source49:   cell_main.conf
Source50:   cell_server.conf
Source51:   set_user_unit.lua
Source52:   build_cell_upstream

Source54:   tmd_check.lua
Source55:   tmd_head
Source56:   tmd_foot


# removes -Werror in upstream build scripts.  -Werror conflicts with
# -D_FORTIFY_SOURCE=2 causing warnings to turn into errors.
Patch0:     %{name}-auto-cc-gcc.patch
Patch1:     tcp.patch

%description
Nginx [engine x] is an HTTP(S) server, HTTP(S) reverse proxy and IMAP/POP3
proxy server written by Igor Sysoev.
Changes: http://baike.corp.taobao.com/index.php/tengine_changelog

%prep
%setup -q

%patch0 -p0
%patch1 -p1

%build
# nginx does not utilize a standard configure script.  It has its own
# and the standard configure options cause the nginx configure script
# to error out.  This is is also the reason for the DESTDIR environment
# variable.  The configure script(s) have been patched (Patch1 and
# Patch2) in order to support installing into a build environment.

# build and install lua
cd luajit
cd src
touch buildvm_*.h
cd ..
make
make install PREFIX=%{buildroot}/luajit
export LUAJIT_LIB=%{buildroot}/luajit/lib
export LUAJIT_INC=%{buildroot}/luajit/include/luajit-2.0

export DESTDIR=%{buildroot}
export WITH_TAIR_LUA=1


# return to nginx directory
cd ..

./configure \
    --user=%{nginx_user} \
    --group=%{nginx_group} \
    --prefix=%{nginx_home_tmp} \
    --sbin-path=%{nginx_sbindir}/%{name} \
    --conf-path=%{nginx_confdir}/%{name}.conf \
    --error-log-path=%{nginx_logdir}/error.log \
    --pid-path=%{nginx_logdir}/%{name}.pid \
    --lock-path=%{nginx_logdir}/%{name}.lock \
    --http-log-path=%{nginx_logdir}/access.log \
    --http-client-body-temp-path=%{nginx_home_tmp}/client_body \
    --http-proxy-temp-path=%{nginx_home_tmp}/proxy \
    --dso-tool-path=%{nginx_sbindir}/dso-tool \
    --with-http_stub_status_module \
    --with-http_ssl_module \
    --with-http_upstream_check_module \
    --without-select_module \
    --without-poll_module \
    --without-http_userid_module \
    --without-http_empty_gif_module \
    --without-http_upstream_consistent_hash_module \
    --with-luajit-inc=%{buildroot}/luajit/include/luajit-2.0 \
    --with-luajit-lib=%{buildroot}/luajit/lib \
    --add-module=mod_lua \
    --add-module=mod_beacon \
    --add-module=mod_cookie_beacon \
    --add-module=mod_trans \
    --add-module=mod_cache_purge \
    --add-module=mod_hat \
    --add-module=mod_tbpass \
    --add-module=mod_diamond \
    --add-module=mod_gray \
    --add-module=mod_tmd \
    --add-module=mod_tair \
    --add-module=mod_tair/tair_2 \
    --add-module=mod_echo \
    --add-module=ngx_devel_kit \
    --add-module=mod_hourglass \
    --add-module=mod_waf \
    --add-module=mod_cookie_pass \
    --add-module=mod_srcache \
    --add-module=mod_chunkin \
    --add-module=mod_delay \
    --add-module=mod_hot \
    --add-module=mod_trim \
    --add-module=mod_eagleeye \
    --add-module=ngx_tcp_proxy_module \
    --add-module=mod_cell_rewrite \
    --add-module=mod_consistent_hash \
    --add-module=mod_gunzip \
    --add-module=mod_bucket_id \
    --add-module=mod_http_copy \
    --add-module=mod_scroll_log \
    --add-module=mod_cdn_variables \
    --with-http_sysguard_module \
    --with-http_concat_module \
    --with-syslog \
    --with-cc-opt="%{optflags} $(pcre-config --cflags)" \
    --with-ld-opt="-luuid"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd luajit
make
make install PREFIX=%{buildroot}/luajit

mkdir -p %{buildroot}%{nginx_libdir}/lua_clib
mkdir -p %{buildroot}%{nginx_confdir}/gray_conf/lua_clib

cd ../lua-clib
gcc -o lua-dir.so -shared lua-dir.c -I%{buildroot}/luajit/include/luajit-2.0 -Wall -O2 -fPIC
cp lua-dir.so %{buildroot}%{nginx_libdir}/lua_clib

cd ../lua
make linux
make install INSTALL_TOP=%{buildroot}/lua

cd ../luaposix
export LUA='%{buildroot}/lua/bin/lua'
CPPFLAGS='-I%{buildroot}/lua/include/' ./configure
make
cp .libs/posix_c.so %{buildroot}%{nginx_libdir}/lua_clib

cd ../lua-cjson
cc -c -O3 -Wall -pedantic -DNDEBUG  -I%{buildroot}/luajit/include/luajit-2.0 -fpic -o lua_cjson.o lua_cjson.c
cc -c -O3 -Wall -pedantic -DNDEBUG  -I%{buildroot}/luajit/include/luajit-2.0 -fpic -o strbuf.o strbuf.c
cc -c -O3 -Wall -pedantic -DNDEBUG  -I%{buildroot}/luajit/include/luajit-2.0 -fpic -o fpconv.o fpconv.c
cc  -shared -o cjson.so lua_cjson.o strbuf.o fpconv.o
mkdir -p %{buildroot}%{nginx_confdir}/tair_rest/lua_clib
cp cjson.so %{buildroot}%{nginx_libdir}/lua_clib

cd ..
rm -rf %{buildroot}/luajit
rm -rf %{buildroot}/lua

make install DESTDIR=%{buildroot} INSTALLDIRS=vendor
find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} \;
find %{buildroot} -type f -empty -exec rm -f {} \;
find %{buildroot} -type d -name html -print | xargs rm -rf
find %{buildroot} -type f -exec chmod 0644 {} \;
find %{buildroot} -type f -name '*.so' -exec chmod 0755 {} \;
find %{buildroot} -type f -name '*.h' -exec chmod 0444 {} \;
chmod 0755 %{buildroot}%{nginx_sbindir}/%{name}
chmod 0755 %{buildroot}%{nginx_sbindir}/dso-tool
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/tmdconf
%{__install} -p -D -m 0644 %{SOURCE54} %{buildroot}%{nginx_confdir}/tmdconf/check.lua
%{__install} -p -D -m 0644 %{SOURCE55} %{buildroot}%{nginx_confdir}/tmdconf/head
%{__install} -p -D -m 0644 %{SOURCE56} %{buildroot}%{nginx_confdir}/tmdconf/foot
%{__install} -p -D -m 0644 %{SOURCE1} %{buildroot}%{nginx_confdir}/%{name}.conf
%{__install} -p -D -m 0644 %{SOURCE3} %{buildroot}%{nginx_confdir}/ip.dat
%{__install} -p -D -m 0644 %{SOURCE4} %{buildroot}%{nginx_confdir}/sm2tr.txt
%{__install} -p -D -m 0644 %{SOURCE5} %{buildroot}%{nginx_confdir}/taobao-beacon.cfg
%{__install} -p -D -m 0644 %{SOURCE6} %{buildroot}%{nginx_confdir}/taobao-channel.cfg
%{__install} -p -d -m 0755 %{buildroot}%{nginx_libdir}/lua_lib
%{__install} -p -D -m 0644 %{SOURCE29} %{buildroot}%{nginx_libdir}/lua_lib/nginx-admin-utils.lua
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/admin
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/admin/lua_script
%{__install} -p -D -m 0644 %{SOURCE28} %{buildroot}%{nginx_confdir}/admin/lua_script/nginx-admin-action.lua
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/apps
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_strategy
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/lua_lib
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/lua_clib
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/lua_script
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/backup
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/test
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/json
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/resource
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/gray_conf/error_confs
%{__install} -p -D -m 0644 %{SOURCE8} %{buildroot}%{nginx_confdir}/gray_conf/lua_script/nginx-gray.lua
%{__install} -p -D -m 0644 %{SOURCE9} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/json.lua
%{__install} -p -D -m 0644 %{SOURCE12} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_taobao.conf
%{__install} -p -D -m 0644 %{SOURCE13} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_beijing.conf
%{__install} -p -D -m 0644 %{SOURCE14} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_hangzhou.conf
%{__install} -p -D -m 0644 %{SOURCE15} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_guangzhou.conf
%{__install} -p -D -m 0644 %{SOURCE16} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_shanghai.conf
%{__install} -p -D -m 0644 %{SOURCE17} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_shenzhen.conf
%{__install} -p -D -m 0644 %{SOURCE40} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_guizhou.conf
%{__install} -p -D -m 0644 %{SOURCE41} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_hubei.conf
%{__install} -p -D -m 0644 %{SOURCE42} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_jiangsu.conf
%{__install} -p -D -m 0644 %{SOURCE43} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_tianjin.conf
%{__install} -p -D -m 0644 %{SOURCE44} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_yunnan.conf
%{__install} -p -D -m 0644 %{SOURCE45} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_henan.conf
%{__install} -p -D -m 0644 %{SOURCE46} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_hunan.conf
%{__install} -p -D -m 0644 %{SOURCE47} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_jilin.conf
%{__install} -p -D -m 0644 %{SOURCE48} %{buildroot}%{nginx_confdir}/gray_conf/resource/geo_zhejiang.conf
%{__install} -p -D -m 0644 %{SOURCE7} %{buildroot}%{nginx_confdir}/gray_conf/resource/gray-config-server.conf
%{__install} -p -d -m 0755 %{buildroot}%{nginx_home_tmp}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_logdir}
%{__install} -p -D -m 0755 %{SOURCE19} %{buildroot}%{_initrddir}/%{name}
%{__install} -p -D -m 0644 %{SOURCE20} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/tair_rest
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/tair_rest/lua_lib
%{__install} -p -d -m 0755 %{buildroot}%{nginx_confdir}/tair_rest/lua_clib
%{__install} -p -D -m 0644 %{SOURCE21} %{buildroot}%{nginx_confdir}/tair_rest/lua_lib/tair.lua
%{__install} -p -D -m 0644 %{SOURCE22} %{buildroot}%{nginx_confdir}/tmd3_main.conf
%{__install} -p -D -m 0644 %{SOURCE23} %{buildroot}%{nginx_confdir}/tmd3_http.conf
%{__install} -p -D -m 0644 %{SOURCE24} %{buildroot}%{nginx_confdir}/tmd3_ip.conf
%{__install} -p -D -m 0644 %{SOURCE25} %{buildroot}%{nginx_confdir}/tmd3_loc.conf
%{__install} -p -D -m 0755 %{SOURCE2} %{buildroot}%{nginx_sbindir}/nginxctl
%{__install} -p -D -m 0755 %{SOURCE30} %{buildroot}%{nginx_sbindir}/nginx-admin-ctl
%{__install} -p -D -m 0644 %{SOURCE26} %{buildroot}%{nginx_confdir}/tmd3_loc_wireless.conf
%{__install} -p -D -m 0644 %{SOURCE27} %{buildroot}%{nginx_confdir}/nginx-admin.conf
%{__install} -p -D -m 0644 %{SOURCE31} %{buildroot}%{nginx_confdir}/gray_conf/lua_script/gray_admin.lua
%{__install} -p -D -m 0644 %{SOURCE32} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/gray_conf.lua
%{__install} -p -D -m 0644 %{SOURCE33} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/gray_utils.lua
%{__install} -p -D -m 0644 %{SOURCE34} %{buildroot}%{nginx_confdir}/gray_conf/lua_script/gray_engine.lua
%{__install} -p -D -m 0644 %{SOURCE35} %{buildroot}%{nginx_confdir}/gray_conf/lua_script/engine_init.lua
%{__install} -p -D -m 0644 %{SOURCE36} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/engine_conf_creator.lua
%{__install} -p -D -m 0644 %{SOURCE37} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/nginx_conf_parser.lua
%{__install} -p -D -m 0644 %{SOURCE38} %{buildroot}%{nginx_confdir}/gray_conf/lua_lib/nginx_operator.lua
%{__install} -p -D -m 0644 %{SOURCE39} %{buildroot}%{nginx_confdir}/gray_conf/resource/gray-log-format.conf
%{__install} -p -D -m 0644 %{SOURCE49} %{buildroot}%{nginx_confdir}/cell_main.conf
%{__install} -p -D -m 0644 %{SOURCE50} %{buildroot}%{nginx_confdir}/cell_server.conf
%{__install} -p -D -m 0644 %{SOURCE51} %{buildroot}%{nginx_confdir}/set_user_unit.lua
%{__install} -p -D -m 0755 %{SOURCE52} %{buildroot}%{nginx_sbindir}/build_cell_upstream

%clean
rm -rf %{buildroot}

%pre
if [ $1 == 1 ]; then
    %{_sbindir}/useradd -c "Nginx user" -s /bin/false -r -d %{nginx_home} %{nginx_user} 2>/dev/null || :
    sed '/%{name}-rlog/'d /etc/crontab -i
fi

%post
if [ $1 == 1 ]; then
    /sbin/chkconfig --add %{nginx_name}
    /sbin/sysctl -w net.unix.max_dgram_qlen=60000
fi

%preun
if [ $1 = 0 ]; then
    /sbin/service %{name} stop >/dev/null 2>&1
    sed '/%{name}-rlog/'d /etc/crontab -i
    sed '/tmd-rlog/'d /etc/crontab -i
    /sbin/chkconfig --del %{nginx_name}
fi

%postun
if [ $1 == 2 ]; then
    /sbin/service %{name} upgrade || :
fi

%files
%defattr(-,%{nginx_user},%{nginx_group},-)
%dir %{nginx_home_tmp}
%dir %{nginx_sbindir}
%{nginx_sbindir}/%{name}
%{nginx_sbindir}/nginxctl
%{nginx_sbindir}/nginx-admin-ctl
%{nginx_sbindir}/build_cell_upstream
%{nginx_sbindir}/dso-tool
%attr(6755, root, admin) %{nginx_sbindir}/%{name}
%attr(0755, root, admin) %{nginx_sbindir}/dso-tool
%dir %{nginx_datadir}
%dir %{nginx_confdir}
%dir %{nginx_confdir}/apps
%{nginx_include}
%{nginx_modules}
%{nginx_confdir}/gray_strategy
%{nginx_confdir}/gray_conf
%{nginx_confdir}/admin
%{nginx_libdir}
%{nginx_confdir}/tair_rest
%dir %{nginx_logdir}
%config(noreplace) %{nginx_confdir}/win-utf
%config(noreplace) %{nginx_confdir}/nginx.conf.default
%config(noreplace) %{nginx_confdir}/mime.types.default
%config(noreplace) %{nginx_confdir}/fastcgi.conf
%config(noreplace) %{nginx_confdir}/fastcgi.conf.default
%config(noreplace) %{nginx_confdir}/fastcgi_params
%config(noreplace) %{nginx_confdir}/fastcgi_params.default
%config(noreplace) %{nginx_confdir}/scgi_params
%config(noreplace) %{nginx_confdir}/scgi_params.default
%config(noreplace) %{nginx_confdir}/uwsgi_params
%config(noreplace) %{nginx_confdir}/uwsgi_params.default
%config(noreplace) %{nginx_confdir}/koi-win
%config(noreplace) %{nginx_confdir}/koi-utf
%config(noreplace) %{nginx_confdir}/%{name}.conf
%config(noreplace) %{nginx_confdir}/nginx-admin.conf
%config(noreplace) %{nginx_confdir}/module_stubs
%config(noreplace) %{nginx_confdir}/mime.types
%config(noreplace) %{nginx_confdir}/ip.dat
%config(noreplace) %{nginx_confdir}/sm2tr.txt
%config(noreplace) %{nginx_confdir}/taobao-beacon.cfg
%config(noreplace) %{nginx_confdir}/taobao-channel.cfg
%config(noreplace) %{nginx_confdir}/browsers
%config(noreplace) %{nginx_confdir}/tmd3_main.conf
%config(noreplace) %{nginx_confdir}/tmd3_http.conf
%config(noreplace) %{nginx_confdir}/tmd3_ip.conf
%config(noreplace) %{nginx_confdir}/tmd3_loc.conf
%config(noreplace) %{nginx_confdir}/tmd3_loc_wireless.conf
%config(noreplace) %{nginx_confdir}/set_user_unit.lua
%config(noreplace) %{nginx_confdir}/cell_main.conf
%config(noreplace) %{nginx_confdir}/cell_server.conf
%config(noreplace) %{nginx_confdir}/tmdconf/check.lua
%config(noreplace) %{nginx_confdir}/tmdconf/head
%config(noreplace) %{nginx_confdir}/tmdconf/foot

%defattr(-,root,root,-)
%{_initrddir}/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}


%changelog
* Wed Aug 28 2013 yuanzhuo.yz <yuanzhuo.yz@taobao.com> - 1.4.6
- update tmd.2.2 to tmd.3.0

* Thu Aug 20 2013 wenjing <wenjing.ywb@taobao.com> - 1.4.6
- updated the consistent hash module, make it the same as tengine-cdn

* Thu Jun 27 2013 weiyue <weiyue@taobao.com> - 1.4.6
- add eagleeye module
- update tengine to 1.4.6

* Thu May 31 2013 weiyue <weiyue@taobao.com> - 1.4.5
- update cell logic

* Thu May 30 2013 weiyue <weiyue@taobao.com> - 1.4.5
- add mod_cell_rewrite

* Tue May 22 2013 wenjing <wenjing.ywb@taobao.com> - 1.4.4
- add mod_diamond

* Tue May 16 2013 wenjing <wenjing.ywb@taobao.com> - 1.4.4
- add mod_hot and mod_trim

* Mon Feb 25 2013 weiyue <weiyue@taobao.com> - 1.4.2
- initial packaging according to nginx-proxy v7879
